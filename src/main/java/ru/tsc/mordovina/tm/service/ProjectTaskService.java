package ru.tsc.mordovina.tm.service;

import ru.tsc.mordovina.tm.api.repository.IProjectRepository;
import ru.tsc.mordovina.tm.api.repository.ITaskRepository;
import ru.tsc.mordovina.tm.api.service.IProjectTaskService;
import ru.tsc.mordovina.tm.exception.empty.EmptyIdException;
import ru.tsc.mordovina.tm.model.Project;
import ru.tsc.mordovina.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private ITaskRepository taskRepository;

    private IProjectRepository projectRepository;

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllTaskByProjectId(projectId);
    }

    @Override
    public Task bindTaskById(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId))
            throw new EmptyIdException();
        return taskRepository.bindTaskToProjectById(projectId, taskId);
    }

    @Override
    public Task unbindTaskById(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId))
            throw new EmptyIdException();
        return taskRepository.unbindTaskById(taskId);
    }

    @Override
    public Project removeById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        Project project = projectRepository.findByIndex(index);
        String projectId = project.getId();
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Project removeByName(final String name) {
        Project project = projectRepository.findByName(name);
        String projectId = project.getId();
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

}

