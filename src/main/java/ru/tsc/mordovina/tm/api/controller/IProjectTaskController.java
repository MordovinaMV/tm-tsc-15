package ru.tsc.mordovina.tm.api.controller;

import ru.tsc.mordovina.tm.model.Project;

public interface IProjectTaskController {

    void showTaskByProjectId();

    void bindTaskToProject();

    void unbindTaskFromProject();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

    void clearProjects();

}
