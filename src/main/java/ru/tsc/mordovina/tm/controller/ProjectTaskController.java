package ru.tsc.mordovina.tm.controller;

import ru.tsc.mordovina.tm.api.controller.IProjectTaskController;
import ru.tsc.mordovina.tm.api.controller.ITaskController;
import ru.tsc.mordovina.tm.api.service.IProjectService;
import ru.tsc.mordovina.tm.api.service.IProjectTaskService;
import ru.tsc.mordovina.tm.api.service.ITaskService;
import ru.tsc.mordovina.tm.exception.empty.EmptyIdException;
import ru.tsc.mordovina.tm.exception.empty.EmptyIndexException;
import ru.tsc.mordovina.tm.exception.empty.EmptyNameException;
import ru.tsc.mordovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.mordovina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.mordovina.tm.model.Project;
import ru.tsc.mordovina.tm.model.Task;
import ru.tsc.mordovina.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectService projectService;

    private final ITaskService taskService;

    private final IProjectTaskService projectTaskService;

    private final ITaskController taskController;

    public ProjectTaskController(IProjectService projectService, ITaskService taskService, IProjectTaskService projectTaskService, ITaskController taskController) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.taskController = taskController;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[ENTER TASK ID]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        final Task taskUpdated = projectTaskService.bindTaskById(projectId, taskId);
        if (taskUpdated == null) throw new TaskNotFoundException();
        System.out.println("[OK]");
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[ENTER TASK ID]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        final Task taskUpdated = projectTaskService.unbindTaskById(projectId, taskId);
        if (taskUpdated == null) throw new TaskNotFoundException();
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByProjectId() {
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        final List<Task> tasks = projectTaskService.findTaskByProjectId(projectId);
        if (tasks.size() <= 0) System.out.println("List is empty");
        for (Task task : tasks)
            taskController.showTask(task);
    }

    @Override
    public void removeProjectById() {
        System.out.println("[ENTER PROJECT ID]");
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectTaskService.removeById(id);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[ENTER PROJECT INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (index == null || index < 0) throw new EmptyIndexException();
        projectTaskService.removeByIndex(index);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[ENTER PROJECT NAME]");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        projectTaskService.removeByName(name);
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

}


