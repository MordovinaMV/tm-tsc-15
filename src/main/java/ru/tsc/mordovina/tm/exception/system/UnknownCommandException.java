package ru.tsc.mordovina.tm.exception.system;

import ru.tsc.mordovina.tm.constant.TerminalConst;
import ru.tsc.mordovina.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Incorrect command. Use ``" + TerminalConst.HELP + "`` to display list of commands.");
    }

}
