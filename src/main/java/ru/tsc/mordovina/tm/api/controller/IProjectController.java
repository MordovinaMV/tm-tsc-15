package ru.tsc.mordovina.tm.api.controller;

import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.model.Project;

public interface IProjectController {

    void showById();

    void showByIndex();

    void showByName();

    void createProject();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateByIndex();

    void updateById();

    void showProjects();

    void clearProjects();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void changeStatusById();

    void changeStatusByIndex();

    void changeStatusByName();

}
